﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class MapChip : MonoBehaviour
    {
        int x;
        int z;
        public int X => x;
        public int Z => z;

        public void Init(int x,int z)
        {
            this.x = x;
            this.z = z;
        }
    }
}
