﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public class WallManager : SingletonMonoBehaviour<WallManager>
    {
        public GameObject wallRc;
        public Transform wallParent;
        static readonly int wallYLength = 3;
        static List<Wall> walls = new List<Wall>();

        public void Init()
        {
            wallParent.SetLocalPositionY(wallYLength * GridManager.gridLength * 0.5f);
        }

        public void MakeWall(int x,int z)
        {
            Vector3 position = new Vector3(x * GridManager.gridLength, 0f, z * GridManager.gridLength);
            Quaternion rotation = Quaternion.identity;
            GameObject obj = Instantiate(wallRc, position, rotation, wallParent);
            obj.transform.localPosition = position;
            obj.transform.localScale = GridManager.gridLength * new Vector3(Wall.XLength, wallYLength, Wall.ZLength);
            Wall wall = new Wall(x, z, obj);
            walls.Add(wall);
        }

        public void MakeWall(Point2 grid)
        {
            MakeWall(grid.X, grid.Z);
        }

        public void LocateWall(Point2 grid)
        {

        }

        /*public void DestroyWall(Wall wall)
        {
            Assert.IsTrue(wall != null);
            bool success = walls.Remove(wall);
            Assert.IsTrue(success);
            Destroy(wall.obj);
        }*/
    }
}