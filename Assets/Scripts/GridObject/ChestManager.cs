﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public class ChestManager : SingletonMonoBehaviour<ChestManager>
    {
        public GameObject chestRc;
        public Transform chestParant;
        public Material nonSelectedRc;
        public Material selectedRc;
        public GameObject jewelRc;
        static List<Chest> chests = new List<Chest>();
        static List<GameObject> jewels = new List<GameObject>();

        public void Init()
        {
            Assert.IsTrue((GridManager.xCount - 1) % 4 == 0);
            int interval = GridManager.xCount / 4;
            for (int i = 1; i <= 3; i++)
            {
                int x = interval * i;
                MakeChest(x, 1);
            }
        }

        void MakeChest(int x, int z)
        {
            Vector3 position = new Vector3(x * GridManager.gridLength, 0f, z * GridManager.gridLength);
            Quaternion rotation = Quaternion.identity;
            GameObject obj = Instantiate(chestRc, position, rotation, chestParant);
            obj.transform.localScale = new Vector3(Chest.XLength * GridManager.gridLength, 20f, Chest.ZLength * GridManager.gridLength);
            Chest chest = new Chest(x, z, obj);
            chests.Add(chest);
        }

        void MakeJewel(int x, int z)
        {
            Vector3 position = new Vector3(x * GridManager.gridLength, 20f, z * GridManager.gridLength);
            Quaternion rotation = Quaternion.identity;
            GameObject obj = Instantiate(jewelRc, position, rotation, chestParant);
            obj.transform.localScale = new Vector3(Chest.XLength * GridManager.gridLength, 30f, Chest.ZLength * GridManager.gridLength);
            jewels.Add(obj);
        }

        public void UpdateChests(int x, int z)
        {
            foreach (Chest chest in chests)
            {
                UpdateChest(chest, x, z);
            }
        }

        void UpdateChest(Chest chest, int x, int z)
        {
            if (chest.Grid.X - 1 <= x && x <= chest.Grid.X + 1 &&
               chest.Grid.Z - 1 <= z && z <= chest.Grid.Z + 1)
            {
                SelectChest(chest);
            }
            else
            {
                DeselectChest(chest);
            }
        }

        void SelectChest(Chest chest)
        {
            chest.obj.GetComponent<MeshRenderer>().material = selectedRc;
            chest.selected = true;
        }

        void DeselectChest(Chest chest)
        {
            chest.obj.GetComponent<MeshRenderer>().material = nonSelectedRc;
            chest.selected = false;
        }

        public bool DecisionChests()
        {
            foreach (Chest chest in chests)
            {
                if (DecisionChest(chest))
                    return true;
            }
            return false;
        }

        bool DecisionChest(Chest chest)
        {
            if (chest.selected)
            {
                DeselectChest(chest);
                MakeJewel(chest.Grid.X, chest.Grid.Z);
                return true;
            }
            return false;
        }
    }
}
