﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public abstract class GridObject
    {
        public int XLength { get; protected set; }
        public int ZLength { get; protected set; }
        public Point2 Grid { get; private set; }
        public GameObject obj;

        abstract protected void SetSize();

        public GridObject(int x,int z,GameObject obj)
        {
            this.obj = obj;
            SetSize();
            InitGrid(x, z);
        }

        public void SetPosition(float x,float z)
        {
            obj.transform.SetLocalPositionXZ(x, z);
        }

        void InitGrid(int x, int z)
        {
            Assert.IsTrue(!GridManager.IsOcupied(x, z));
            Grid = new Point2(x, z);
            GridManager.OccupyGrid(this);
            obj.transform.localPosition = new Vector3(x * GridManager.gridLength, obj.transform.localPosition.y, z * GridManager.gridLength);
        }
    }
}
