﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class Wall : GridObject
    {
        public static new readonly int XLength = 1;
        public static new readonly int ZLength = 1;
        int health = 10;

        protected override void SetSize()
        {
            base.XLength = XLength;
            base.ZLength = ZLength;
        }

        public Wall(int x, int z, GameObject obj) : base(x, z, obj) { }


    }
}
