﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public class GridManager : SingletonMonoBehaviour<GridManager>
    {
        public static int xCount = 21;
        public static int zCount = 30;
        public readonly static float gridLength = 10f;
        static bool[,] occupied = new bool[xCount, zCount];

        public static void OccupyGrid(GridObject obj, bool b = true)
        {
            int lowerLeftX = obj.Grid.X - obj.XLength / 2;
            int lowerLeftZ = obj.Grid.Z - obj.ZLength / 2;
            Point2 lowerLeft = new Point2(lowerLeftX, lowerLeftZ);
            for (int z = 0; z < obj.ZLength; z++)
            {
                for (int x = 0; x < obj.XLength; x++)
                {
                    int occupiedX = lowerLeft.X + x;
                    int occupiedZ = lowerLeft.Z + z;
                    //Debug.Log("x" + occupiedX.ToString() + "z" + occupiedZ.ToString());
                    occupied[occupiedX, occupiedZ] = b;
                }
            }
        }

        public static bool IsOcupied(int x, int z)
        {
            return occupied[x, z];
        }

        public static bool IsOcupied(Point2 dst)
        {
            return occupied[dst.X, dst.Z];
        }

        public static Vector3 ToVector3(Point2 grid)
        {
            return new Vector3(grid.X * gridLength, 0f, grid.Z * gridLength);
        }
    }
}
