﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public partial class Actor : GridObject
    {
        public new static readonly int XLength = 1;
        public new static readonly int ZLength = 1;
        IdleController idleController;
        WalkingControler walkingController;
        FightingWall fightingWall;
        Controller controller;
        Rigidbody rigidbody;
        Collider collider;
        Animator animator;
        private Vector3 Position
        {
            get
            {
                return rigidbody.position;
            }
            set
            {
                rigidbody.position = value;
            }
        }
        private Quaternion Rotation
        {
            get
            {
                return rigidbody.rotation;
            }
            set
            {
                rigidbody.rotation = value;
            }
        }

        protected override void SetSize()
        {
            base.XLength = XLength;
            base.ZLength = ZLength;
        }

        public Actor(int x, int z, GameObject obj) : base(x, z, obj)
        {
            rigidbody = obj.GetComponent<Rigidbody>();
            collider = obj.GetComponent<Collider>();
            animator = obj.transform.Find("AnimatorRoot").GetComponent<Animator>();
            animator.SetTrigger("Idle");
            idleController = new IdleController(this);
            walkingController = new WalkingControler(this);
            fightingWall = new FightingWall(this);
            controller = idleController;
        }

        void StartIdle()
        {
            controller = idleController;
            animator.SetTrigger("Idle");
        }

        public void StartWalking(Point2 dst)
        {
            walkingController.Stert(dst);
        }

        public void UpdateMe()
        {
            controller.UpdateMe();
        }
    }

    public partial class Actor : GridObject
    {
        public abstract class Controller
        {
            protected Actor actor;
            public Controller(Actor actor)
            {
                this.actor = actor;
            }
            public virtual void UpdateMe() { }
        }
    }

    public partial class Actor
    {
        class IdleController : Controller
        {
            public IdleController(Actor actor) : base(actor) { }
        }
    }

    public partial class Actor
    {
        class WalkingControler : Controller
        {
            static readonly float gridPerSec = 2f * GridManager.gridLength;
            static readonly float degreePerSec = 360f;
            Vector3 target;

            public WalkingControler(Actor actor) : base(actor) { }

            public void Stert(Point2 dst)
            {
                if (actor.controller != this)
                {
                    actor.controller = this;
                    actor.animator.SetTrigger("Walk");
                }
                target = GridManager.ToVector3(dst);
            }

            void Finish()
            {
                actor.StartIdle();
            }

            public override void UpdateMe()
            {
                Vector3 current = actor.Position;
                //回転
                Vector3 rotVector = target - current;
                Quaternion targetRot = Quaternion.LookRotation(rotVector);
                float maxDegreeDelta = degreePerSec * Time.fixedDeltaTime;
                actor.Rotation = Quaternion.RotateTowards(actor.Rotation, targetRot, maxDegreeDelta);
                //移動
                float maxDistanceDelta = gridPerSec * Time.fixedDeltaTime;
                actor.Position = Vector3.MoveTowards(current, target, maxDistanceDelta);
                if (actor.Position == target)
                    Finish();
            }
        }
    }

    public partial class Actor
    {
        public class FightingWall : Controller
        {
            static readonly int attack = 3;
            static readonly float animationTime = 0.52f;//適当
            Wall targetWall;
            float attakingTime = 0f;

            public FightingWall(Actor actor) : base(actor) { }

            public void Start(Wall wall)
            {
                if (actor.controller != this)
                {
                    targetWall = wall;
                    actor.animator.SetTrigger("RoundKick");
                    attakingTime = 0f;
                }
            }

            void Finish()
            {
                //壁にダメージ
                actor.StartIdle();
            }

            public override void UpdateMe()
            {
                attakingTime += Time.fixedDeltaTime;
                if (animationTime <= attakingTime)
                    Finish();
            }
        }
    }
}
