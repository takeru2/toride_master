﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class MapChipManager : SingletonMonoBehaviour<MapChipManager>
    {
        public GameObject mapChipRc;
        public Transform mapChipParent;
        public GameObject gridBorderRc;
        public Transform gridBorderParent;
        public readonly static float mapWidth = GridManager.gridLength * (GridManager.xCount + 1);
        public readonly static float mapHeight = GridManager.gridLength * (GridManager.zCount + 1);
        public readonly static float mapWidthCentar = GridManager.gridLength * GridManager.xCount / 2f;
        public readonly static float mapHeightCentar = GridManager.gridLength * GridManager.zCount / 2f;

        static MapChip[,] mapChips = new MapChip[GridManager.xCount, GridManager.zCount];
        static GameObject[,] gridBorders = new GameObject[GridManager.xCount, GridManager.zCount];

        public void Init()
        {
            for (int x = 0; x < GridManager.xCount; x++)
            {
                for (int z = 0; z < GridManager.zCount; z++)
                {
                    MakeMapChip(x, z);
                    MakeGridBorder(x, z);
                }
            }
        }

        void MakeMapChip(int x, int z)
        {
            Vector3 position = new Vector3(x * GridManager.gridLength, 0f, z * GridManager.gridLength);
            Quaternion rotation = Quaternion.identity;
            GameObject obj = Instantiate(mapChipRc, position, rotation, mapChipParent);
            MapChip mapChip = obj.GetComponent<MapChip>();
            mapChip.Init(x, z);
            mapChips[x, z] = mapChip;
        }

        void MakeGridBorder(int x, int z)
        {
            Vector3 position = new Vector3(x * GridManager.gridLength, 0f, z * GridManager.gridLength);
            Quaternion rotation = Quaternion.identity;
            GameObject obj = Instantiate(gridBorderRc, position, rotation, gridBorderParent);
            gridBorders[x, z] = obj;
        }
    }
}
