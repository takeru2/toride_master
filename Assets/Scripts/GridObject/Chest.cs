﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class Chest : GridObject
    {
        public new static readonly int XLength = 3;
        public new static readonly int ZLength = 3;
        public bool selected;

        protected override void SetSize()
        {
            base.XLength = XLength;
            base.ZLength = ZLength;
        }

        public Chest(int x, int z, GameObject obj) : base(x, z, obj)
        {
            ;
        }
    }
}
