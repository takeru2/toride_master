﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public class ActorManager : SingletonMonoBehaviour<ActorManager>
    {
        public GameObject actorRc;
        public Transform actorParent;
        List<Actor> actors = new List<Actor>();
        public static Actor MyActor { get; private set; }

        Actor MakeActor(int x, int z)
        {
            Vector3 position = new Vector3(x * GridManager.gridLength, 0f, z * GridManager.gridLength);
            Quaternion rotation = Quaternion.identity;
            GameObject obj = Instantiate(actorRc, position, rotation, actorParent);
            obj.transform.localScale = new Vector3(Actor.XLength * GridManager.gridLength, 10f, Actor.ZLength* GridManager.gridLength);
            Actor actor = new Actor(x, z, obj);
            actors.Add(actor);
            MyActor = actor;
            return actor;
        }

        public Actor MakeActor(Point2 point2)
        {
            return MakeActor(point2.X, point2.Z);
        }

        public void UpdateMe()
        {
            foreach(Actor actor in actors)
            {
                actor.UpdateMe();
            }
        }
    }
}
