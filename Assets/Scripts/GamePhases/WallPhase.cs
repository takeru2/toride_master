﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace TorideMaster
{
    public partial class GameStateManager
    {
        public class WallPhase : GamePhase
        {
            static readonly int initialWallPoint = 10;
            int wallPoint;//壁を作るごとに1減る
            bool rocked;
            Slider slider;

            public WallPhase()
            {
                wallPoint = initialWallPoint;
                slider = UIManager.Instance.slider;
                InitSlider();
            }

            public override void UpdateMe()
            {
                if (rocked)
                {
                    NextPhase();
                    return;
                }
                TouchInfo touchInfo = InputManager.TouchState;
                Point2? grid = RayController.GetTouchGrid();
                switch (touchInfo)
                {
                    case TouchInfo.None:
                        break;
                    case TouchInfo.Began:
                    case TouchInfo.Moved:
                    case TouchInfo.Stationary:
                        {
                            if (grid == null)
                                break;
                            else
                            {
                                GridCursor.Start(grid.Value);
                                break;
                            }
                        }
                    case TouchInfo.Ended:
                    case TouchInfo.Canceled:
                        {
                            GridCursor.Finish();
                            if (grid == null)
                                break;
                            else
                            {
                                if (GridManager.IsOcupied(grid.Value))
                                {
                                    break;
                                }
                                else
                                {
                                    MakeWall(grid.Value);
                                }
                                break;
                            }
                        }
                    default:
                        Assert.IsTrue(false);
                        break;
                }
            }

            void MakeWall(Point2 grid)
            {
                Assert.IsTrue(0 < wallPoint);
                WallManager.Instance.MakeWall(grid);
                wallPoint--;
                AdjustSliderValue();
                if (wallPoint == 0)
                    rocked = true;
            }

            void InitSlider()
            {
                slider.maxValue = initialWallPoint;
                slider.minValue = 0;
                AdjustSliderValue();
            }

            void AdjustSliderValue()
            {
                slider.value = wallPoint;
            }
        }
    }
}
