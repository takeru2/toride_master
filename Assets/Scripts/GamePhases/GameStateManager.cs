﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TorideMaster
{
    public partial class GameStateManager : SingletonMonoBehaviour<GameStateManager>
    {
        static int index;
        static List<GamePhase> gamePhases = new List<GamePhase>();

        public static void Init()
        {
            gamePhases.Add(new ChestPhase());
            gamePhases.Add(new ActorPhase());
            gamePhases.Add(new WallPhase());
            gamePhases.Add(new BattlePhase());
            index = 0;
        }

        static public void UpdateMe()
        {
            gamePhases[index].UpdateMe();
        }

        static public void FixedUpdateMe()
        {
            gamePhases[index].FixedUpdateMe();
        }

        static void NextPhase()
        {
            index++;
            if (index - 1 == gamePhases.Count)
                index = 0;
        }
    }
}
