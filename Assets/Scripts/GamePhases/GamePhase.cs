﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public partial class GameStateManager
    {
        public abstract class GamePhase
        {
            public virtual void UpdateMe() { }
            public virtual void FixedUpdateMe() { }
        }
    }
}