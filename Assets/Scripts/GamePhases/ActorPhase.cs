﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public partial class GameStateManager
    {
        public class ActorPhase : GamePhase
        {
            bool rocked;

            public override void UpdateMe()
            {
                if (rocked)
                {
                    NextPhase();
                    return;
                }
                TouchInfo touchInfo = InputManager.TouchState;
                Point2? grid = RayController.GetTouchGrid();
                switch (touchInfo)
                {
                    case TouchInfo.None:
                        break;
                    case TouchInfo.Began:
                    case TouchInfo.Stationary:
                    case TouchInfo.Moved:
                        if (grid == null)
                            break;
                        GridCursor.Start(grid.Value);
                        break;
                    case TouchInfo.Canceled:
                    case TouchInfo.Ended:
                        GridCursor.Finish();
                        if (grid == null || GridManager.IsOcupied(grid.Value))
                        {
                            break;
                        }
                        else
                        {
                            ActorManager.Instance.MakeActor(grid.Value);
                            rocked = true;
                            break;
                        }
                    default:
                        Assert.IsTrue(false);
                        break;
                }
            }
        }
    }
}
