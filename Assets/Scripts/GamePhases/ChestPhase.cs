﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public partial class GameStateManager
    {
        public class ChestPhase : GamePhase
        {
            bool rocked;

            public override void UpdateMe()
            {
                if (rocked)
                {
                    NextPhase();
                    return;
                }
                TouchInfo touchInfo = InputManager.TouchState;
                Point2? grid = RayController.GetTouchGrid();
                switch (touchInfo)
                {
                    case TouchInfo.None:
                        break;
                    case TouchInfo.Began:
                    case TouchInfo.Stationary:
                    case TouchInfo.Moved:
                        if (grid == null)
                            return;
                        GridCursor.Start(grid.Value);
                        SelectGrid(grid.Value.X, grid.Value.Z);
                        break;
                    case TouchInfo.Canceled:
                    case TouchInfo.Ended:
                        GridCursor.Finish();
                        Deselect();
                        break;
                    default:
                        Assert.IsTrue(false);
                        break;
                }
            }

            void SelectGrid(int x, int z)
            {
                if (rocked)
                    return;
                else
                    ChestManager.Instance.UpdateChests(x, z);
            }

            void Deselect()
            {
                if (rocked)
                    return;
                else
                    rocked = ChestManager.Instance.DecisionChests();
            }
        }
    }
}
