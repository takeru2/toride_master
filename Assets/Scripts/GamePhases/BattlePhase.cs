﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public partial class GameStateManager
    {
        public class BattlePhase : GamePhase
        {
            /*GameObject targetGrid;

            public BattlePhase()
            {
                targetGrid = (GameObject)Resources.Load("Prefabs/TargetGrid", typeof(GameObject));
            }*/

            public override void UpdateMe()
            {
                TouchInfo touchInfo = InputManager.TouchState;
                Point2? grid = RayController.GetTouchGrid();
                switch (touchInfo)
                {
                    case TouchInfo.None:
                        break;
                    case TouchInfo.Began:
                    case TouchInfo.Stationary:
                    case TouchInfo.Moved:
                        if (grid == null)
                            break;
                        GridCursor.Start(grid.Value);
                        break;
                    case TouchInfo.Canceled:
                    case TouchInfo.Ended:
                        GridCursor.Finish();
                        if (grid == null)
                            break;
                        else
                        {
                            ActorManager.MyActor.StartWalking(grid.Value);
                        }
                        //旗を置く
                        break;
                }
            }

            public override void FixedUpdateMe()
            {
                ActorManager.Instance.UpdateMe();
            }
        }
    }
}
