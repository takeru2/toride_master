﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class CameraController : SingletonMonoBehaviour<CameraController>
    {
        public static Camera mainCamera;

        public void Init()
        {
            mainCamera = Camera.main;
            /*float frustumHeight = MapChipManager.mapHeight;
            mainCamera.farClipPlane = GetDistance(frustumHeight);
            mainCamera.transform.SetLocalPositionY(mainCamera.farClipPlane);
            SetCameraXZ();*/
        }

        float GetDistance(float frustumHeight)
        {
            float result = frustumHeight * 0.5f / Mathf.Tan(mainCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);
            return result;
        }

        //クライアント毎に異なる
        void SetCameraXZ()
        {
            float x = MapChipManager.mapWidthCentar;
            mainCamera.transform.SetLocalPositionX(x);
            float z = MapChipManager.mapHeightCentar;
            mainCamera.transform.SetLocalPositionZ(z);
        }
    }
}
