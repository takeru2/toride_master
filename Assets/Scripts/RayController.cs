﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace TorideMaster
{
    public class RayController : SingletonMonoBehaviour<RayController>
    {
        static Point2? RayToGrid(Vector3 vector3)
        {
            Ray ray = CameraController.mainCamera.ScreenPointToRay(vector3);
            RaycastHit hitInfo;
            Physics.Raycast(ray, out hitInfo,Mathf.Infinity,LayerMask.GetMask("Grid"));
            if (hitInfo.collider == null)
                return null;
            if (hitInfo.collider.tag == "mapChip")
            {
                MapChip mapChip = hitInfo.collider.gameObject.GetComponent<MapChip>();
                return new Point2(mapChip.X, mapChip.Z);
            }
            Assert.IsTrue(false);
            return null;
        }

        public static Point2? GetTouchGrid()
        {
            Vector3 touchPositon = InputManager.TouchPosition;
            return RayToGrid(touchPositon);
        }
    }
}
