﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class Root : SingletonMonoBehaviour<Root>
    {
        void Awake()
        {
            MapChipManager.Instance.Init();
            CameraController.Instance.Init();
            ChestManager.Instance.Init();
            WallManager.Instance.Init();
            GameStateManager.Init();
            GridCursor.Instance.Init();
        }

        void Update()
        {
            InputManager.UpdateMe();
            GameStateManager.UpdateMe();
        }

        void FixedUpdate()
        {
            GameStateManager.FixedUpdateMe();
        }
    }
}
