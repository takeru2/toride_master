﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public struct Point2
    {
        public int X { get; private set; }
        public int Z { get; private set; }

        public Point2(int x, int z)
        {
            this.X = x;
            this.Z = z;
        }

        public override string ToString()
        {
            return "x =" + X.ToString() + ",z =" + Z.ToString();
        }
    }
}
