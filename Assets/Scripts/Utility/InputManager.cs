﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public static class InputManager
{
    static float screenHeight = Screen.height;
    static float screenWidth = Screen.width;
    static Vector2 canvasResolution = new Vector2(640f, 1136f);

    public static TouchInfo TouchState { get; private set; }
    public static Vector3 TouchPosition { get; private set; }

    public static void UpdateMe()
    {
        TouchState = UpdateTouchInfo();
        TouchPosition = UpdateTouchPosition();
    }

    static TouchInfo UpdateTouchInfo()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                return TouchInfo.Began;
            }
            if (Input.GetMouseButton(0))
            {
                return TouchInfo.Moved;
            }
            if (Input.GetMouseButtonUp(0))
            {
                return TouchInfo.Ended;
            }
        }
        else
        {
            if (0 < Input.touchCount)
            {
                return (TouchInfo)((int)Input.GetTouch(0).phase);
            }
        }
        return TouchInfo.None;
    }

    static Vector3 UpdateTouchPosition()
    {
        if (Application.isEditor)
        {
            TouchInfo touch = UpdateTouchInfo();
            if (touch != TouchInfo.None)
            {
                float widthRatio = canvasResolution.x / Screen.width;
                float heightRatio = canvasResolution.y / Screen.height;
                float newX = widthRatio * Input.mousePosition.x;
                float newY = heightRatio * Input.mousePosition.y;
                return new Vector3(newX, newY, 0f);
            }
            else
                return Vector3.zero;
        }
        else
        {
            if (0 < Input.touchCount)
            {
                float widthRatio = canvasResolution.x / screenWidth;
                float heightRatio = canvasResolution.y / screenHeight;
                Touch touch = Input.GetTouch(0);
                float newX = widthRatio * touch.position.x;
                float newY = heightRatio * touch.position.y;
                return new Vector3(newX, newY, 0f);
            }
            else
                return Vector3.zero;
        }
    }

    public static Vector3 GetTouchWorldPosition(Camera camera)
    {
        return camera.ScreenToWorldPoint(UpdateTouchPosition());
    }
}
public enum TouchInfo
{
    None = 99,
    Began = 0,
    Moved = 1,
    Stationary = 2,
    Ended = 3,
    Canceled = 4,
}