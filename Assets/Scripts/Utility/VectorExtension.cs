﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtension
{
    public static void SetX(this Vector3 vector3, float x)
    {
        vector3.Set(x, vector3.y, vector3.z);
    }

    public static void SetY(this Vector3 vector3, float y)
    {
        vector3.Set(vector3.x, y, vector3.z);
    }

    public static void SetZ(this Vector3 vector3, float z)
    {
        vector3.Set(vector3.x, vector3.y, z);
    }

    public static void SetXY(this Vector3 vector3, float x, float y)
    {
        vector3.Set(x, y, vector3.z);
    }

    public static void SetXZ(this Vector3 vector3, float x, float z)
    {
        vector3.Set(x, vector3.y, z);
    }

    public static void SetYZ(this Vector3 vector3, float y, float z)
    {
        vector3.Set(vector3.x, y, z);
    }
}
