﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TorideMaster
{
    public class GridCursor : SingletonMonoBehaviour<GridCursor>
    {
        public GameObject cursorRc;
        public Transform cursorParent;
        static GameObject cursor;

        public void Init()
        {
            cursor = Instantiate(cursorRc, cursorParent);
            SetActive(false);
        }

        public static void Start(Point2 grid)
        {
            SetActive(true);
            SetGrid(grid);
        }

        public static void Finish()
        {
            SetActive(false);
        }

        public static void SetGrid(Point2 grid)
        {
            cursor.transform.localPosition = new Vector3(grid.X * GridManager.gridLength, 0, grid.Z * GridManager.gridLength);
        }

        static void SetActive(bool b)
        {
            cursor.SetActive(b);
        }
    }
}
